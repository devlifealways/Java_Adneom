package ednoem.test;

/**
 * @author ROUINEB Hamza, rouineb.work@gmail.com/0785087523
 * @version 0.0.1
 * 
 * General Idea : for simplicity reasons, I didn't use Integer class wrapper 
 * even if it's more flexible (the idea in first place was to work with the collection's framework)
 * 
 */

public class Handler implements Utility {

	/**
	 * Display the content of the 2d array in a simple way
	 * 
	 * @param input
	 *            2d array of integers
	 */
	public static void displayArray(int[][] input) {
		System.out.print("[ ");
		for (int i = 0; i < input.length; ++i) {
			System.out.print("[");
			for (int j = 0; j < input[i].length; ++j) {
				System.out.print(input[i][j]);
				if (j + 1 != input[i].length)
					System.out.print(",");

			}
			System.out.print("]");
			if (i + 1 != input.length)
				System.out.print(",");
		}
		System.out.print(" ]");
	}

	public static void main(String... args) {

		// this is a simple example, please feel free to change it to whatever
		// you want
		int tab[] = { 1, 2, 3, 77, 44, 89, 232, 434 };

		try {
			Utility handler = new Handler();
			int[][] output = handler.parition(tab, 3);

			displayArray(output);

			System.out.println("");

		} catch (Exception ex) {
			// perhaps you might do some handy work here ?!
			ex.printStackTrace();
		}
	}

}
