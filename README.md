This a simple application made in Java 8. for the purpose of solving a puzzle.
please make sure before running the build phase, that you have maven (build tool) and the appropriate Java SDK version (1.8)

# To run the build without tests : 
    $ mvn clean install -DskipTests
# With the tests, in this case, you'll have two generated reports with the extensions (xml & txt) in which you can find useful data. 
    $ mvn clean install 

# Finally, if you want to launch the packaged app as one entity (jar), you'll have to run the following commands :
    $ java -jar target/EDNOEM_ROUINEB.jar

PS : you'll have to be in the root folder to run the application.